package com.tsystems.javaschool.tasks.calculator;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
		
		if( statement == null )return null;
		if( statement == "" )return null;	
		
		char[] statementCharArray = statement.toCharArray();
		int i, j;;
		
		float value = 0;
		float v;
		float mod;
		String result;
		
		float[] valuesStack = new float[ statementCharArray.length ]; 
		int valuesStackTopElementIndex = -1;
		
		char[] operatorsStack = new char[ statementCharArray.length ]; 
		int operatorsStackTopElementIndex = -1;
		
		String valueString = "";
		
		StringBuffer valueStringBuffer = new StringBuffer();
		
		int dotCharsCount = 0;
		
		boolean hasStackOperatorPrecedence = false;
		

		
		
		for( i=0; i < statementCharArray.length; i++ ) {
System.out.println( "///" );
System.out.println( statementCharArray[i] );
System.out.println( "values stack:" );				
for( j=0; j <= valuesStackTopElementIndex; j++  ){						
	System.out.println( valuesStack[j] );
}
System.out.println( "ops stack:" );				
for( j=0; j <= operatorsStackTopElementIndex; j++  ){						
	System.out.println( operatorsStack[j] );
}
System.out.println( "///" );			
			switch( statementCharArray[i] ) {
				case ' ': break;
				case '(':			
					operatorsStackTopElementIndex++;
					operatorsStack[ operatorsStackTopElementIndex ] = statementCharArray[i];
					break;
				case ')':{
					while ( operatorsStackTopElementIndex >= 0 && operatorsStack[ operatorsStackTopElementIndex ] != '(' ) {
						if( valuesStackTopElementIndex < 1 ) return null;
						switch ( operatorsStack[ operatorsStackTopElementIndex ] ) { 
							case '+': 
								value = valuesStack[ valuesStackTopElementIndex - 1 ] + valuesStack[ valuesStackTopElementIndex ]; 
								break;	
							case '-': 
								value = valuesStack[ valuesStackTopElementIndex - 1 ] - valuesStack[ valuesStackTopElementIndex ];
								break;
							case '*': 
								value = valuesStack[ valuesStackTopElementIndex - 1 ] * valuesStack[ valuesStackTopElementIndex ];
								break;	
							case '/': 
								if ( valuesStack[ valuesStackTopElementIndex ] == 0 ) {
									return null;
								}
								else {
									value = valuesStack[ valuesStackTopElementIndex - 1] / valuesStack[ valuesStackTopElementIndex ];
									break;	 
								}
							default: return null;
						}

						operatorsStackTopElementIndex--;
						valuesStackTopElementIndex -= 2;
						valuesStackTopElementIndex++;
						valuesStack[ valuesStackTopElementIndex ] = value;
					
					}
					if( operatorsStackTopElementIndex <= 0 && operatorsStack[ operatorsStackTopElementIndex ] != '(' ) return null;
					operatorsStackTopElementIndex--;

					break;
				}
				case '.':
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9': {
					valueStringBuffer.append(statementCharArray[i]);
					i++;
					while ( i < statementCharArray.length && ( (statementCharArray[i] >= '0' && statementCharArray[i] <= '9') || statementCharArray[i]=='.') ) {
						if( statementCharArray[i] == '.' ) dotCharsCount++;
						valueStringBuffer.append(statementCharArray[i]);
						i++;
					}
					i--;
					valueString = valueStringBuffer.toString();
					valueStringBuffer.delete(0, valueStringBuffer.length());


					if( dotCharsCount > 1 ){
						System.out.println ("more then one dot in a number!");
						return null;
					}
					else {
						valuesStackTopElementIndex++;					
						valuesStack[ valuesStackTopElementIndex ] = Float.valueOf(valueString);

					}
					dotCharsCount = 0;				
					break;
				}
				case '+':
				case '-':
				case '*':
				case '/': {
					if( i > 1 && ( statementCharArray[i-1] == '+' || statementCharArray[i-1] == '-' || statementCharArray[i-1] == '*' || statementCharArray[i-1] == '/' ) ) return null;
					if( operatorsStackTopElementIndex >= 0 ) {
						
						hasStackOperatorPrecedence = true;
						if ( operatorsStack[ operatorsStackTopElementIndex ] == '(' || operatorsStack[ operatorsStackTopElementIndex ] == ')' ) 
							hasStackOperatorPrecedence = false; 
						if ( (statementCharArray[i] == '*' || statementCharArray[i] == '/') && (operatorsStack[ operatorsStackTopElementIndex ] == '+' || operatorsStack[ operatorsStackTopElementIndex ] == '-') ) 
						hasStackOperatorPrecedence =  false; 
						
						while(  hasStackOperatorPrecedence && operatorsStackTopElementIndex >= 0 ) {
							switch ( operatorsStack[ operatorsStackTopElementIndex ] ) { 
								case '+': 
									value = valuesStack[ valuesStackTopElementIndex - 1 ] + valuesStack[ valuesStackTopElementIndex ]; 
									break;	
								case '-': 
									value = valuesStack[ valuesStackTopElementIndex - 1 ] - valuesStack[ valuesStackTopElementIndex ];
									break;
								case '*': 
									value = valuesStack[ valuesStackTopElementIndex - 1 ] * valuesStack[ valuesStackTopElementIndex ];
									break;	
								case '/': 
									if ( valuesStack[ valuesStackTopElementIndex ] == 0 ) {
										return null;
									}
									else {
										value = valuesStack[ valuesStackTopElementIndex - 1] / valuesStack[ valuesStackTopElementIndex ];
										break;	 
									}
								default: return null;
							}
							operatorsStackTopElementIndex--;
							valuesStackTopElementIndex -= 2;
							valuesStackTopElementIndex++;
							valuesStack[ valuesStackTopElementIndex ] = value;
							if( operatorsStackTopElementIndex >= 0 ) {
								hasStackOperatorPrecedence = true;
								if ( operatorsStack[ operatorsStackTopElementIndex ] == '(' || operatorsStack[ operatorsStackTopElementIndex ] == ')' ) 
									hasStackOperatorPrecedence = false; 
								if ( (statementCharArray[i] == '*' || statementCharArray[i] == '/') && (operatorsStack[ operatorsStackTopElementIndex ] == '+' || operatorsStack[ operatorsStackTopElementIndex ] == '-') ) 
								hasStackOperatorPrecedence =  false;
							}
						}
					}
					operatorsStackTopElementIndex++;
					operatorsStack[ operatorsStackTopElementIndex ] = statementCharArray[i];


					break;
				}
				
				default: return null;
			}
		}
		
		while( operatorsStackTopElementIndex >= 0 ) {
			switch ( operatorsStack[ operatorsStackTopElementIndex ] ) { 
				case '+': 
					value = valuesStack[ valuesStackTopElementIndex - 1 ] + valuesStack[ valuesStackTopElementIndex ]; 
					break;	
				case '-': 
					value = valuesStack[ valuesStackTopElementIndex - 1 ] - valuesStack[ valuesStackTopElementIndex ];
					break;
				case '*': 
					value = valuesStack[ valuesStackTopElementIndex - 1 ] * valuesStack[ valuesStackTopElementIndex ];
					break;	
				case '/': 
					if ( valuesStack[ valuesStackTopElementIndex ] == 0 ) {
						return null;
					}		
					else {
						value = valuesStack[ valuesStackTopElementIndex - 1] / valuesStack[ valuesStackTopElementIndex ];
						break;	 
					}
				default: return null;
			}
			operatorsStackTopElementIndex--;
			valuesStackTopElementIndex -= 2;
			valuesStackTopElementIndex++;
			valuesStack[ valuesStackTopElementIndex ] = value;
System.out.println( "///" );
System.out.println( "values stack:" );				
for( j=0; j <= valuesStackTopElementIndex; j++  ){						
	System.out.println( valuesStack[j] );
}
System.out.println( "ops stack:" );				
for( j=0; j <= operatorsStackTopElementIndex; j++  ){						
	System.out.println( operatorsStack[j] );
}	
System.out.println( "///" );			
		}
		value = (float) java.lang.Math.round( value * 10000 ) / 10000;
		v = value * 10;
		mod = v % 10;
		if( mod == 0){
			result = Integer.toString( (int)value );
		}
		else{
			result = Float.toString(value);
		}
		
		System.out.println( "---" );
		System.out.println( result );
        return result;
    }

}
