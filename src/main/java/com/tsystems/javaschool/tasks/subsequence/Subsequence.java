package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
		
		if( x == null || y == null ) {
			throw new IllegalArgumentException("");
		}
		
		int xElementIndex;;
		int yElementIndex = 0;
		int xSize = x.size();
		int ySize = y.size();
		if( ySize == 0 && xSize > 0 ) return false;

        for( xElementIndex = 0; xElementIndex < xSize; xElementIndex++ ) {
			while( x.get( xElementIndex ) != y.get( yElementIndex ) ) {
				yElementIndex++;
				if( yElementIndex  >= ySize ) return false;
			}
			
		}
        return true;
    }
}
